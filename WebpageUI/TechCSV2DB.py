import csv
import psycopg2

conn = psycopg2.connect(database="skill_portal", host="localhost", user="postgres", password="root")
cur = conn.cursor()

with open('final_classification.csv', 'r', encoding="utf8") as f:
    reader = csv.reader(f)
    next(reader)
    for row in reader:
        row[1] = row[1].upper().strip()
        print("********",row)
        cur.execute("INSERT INTO resumeparser_technology VALUES (%s, %s, %s, %s, %s, %s, %s);",row)
conn.commit()
