# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

# How to work with BitBucket

Check whether Git is available

Open a terminal and verify the installation
$ git --version
    git version 2.9.2, if you see this (version may vary) git is available in your system, else 
Install Git

Clone the repository

Go to the dashboard
Click on Repository
Choose the repository
Copy the repo url
use that repository url to clone

$ git clone <repository url>

eg: git clone https://amal-8642@bitbucket.org/crew_data/crew-analytics.git
Cloning the repo creates a copy of the repository locally, with the repository on your local system, you can add, or edit the files
To add the files or changes to the repository

$ git add --all
To see the status of the working directory eg : Files to be commited, untracked files

$ git status
To commit new files/changes to the local repository

$ git commit -m "<commit_message>"
To upload local repository content to a remote repository

$ git push
To fetch and download content from a remote repository and immediately update the local repository to match that content

$ git pull <remote>
This is the same as:
$ git fetch <remote>
followed by:
$ git merge origin/<current-branch>.

