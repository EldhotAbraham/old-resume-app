      function loadSankey(){
          google.charts.load('current', {'packages':['sankey']});
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'From');
            data.addColumn('string', 'To');
            data.addColumn('number', 'Weight');
            data.addRows([
              [ 'Chat', 'Flight Reservation', 20 ],
              [ 'Chat', 'Check Weather', 40 ],
              [ 'Mail', 'Flight Reservation', 5 ],
              [ 'Voice', 'Flight Reservation', 50 ],
              [ 'Voice', 'Check Weather', 60],
              [ 'Chat', 'Retro Claim', 30 ],
              [ 'Mail', 'Retro Claim', 60 ],
              [ 'Voice', 'Retro Claim', 5 ],
              [ 'Chat', 'Missed Baggage', 50 ],
              [ 'Mail', 'Missed Baggage', 4 ],
              [ 'Voice', 'Missed Baggage', 30 ],
              [ 'Tweet', 'Flight Delay', 30 ],
              [ 'Tweet', 'Weather', 50 ],
            ]);

            // Instantiates and draws our chart, passing in some options.
            var chart = new google.visualization.Sankey(document.getElementById('sankey_chart'));
            chart.draw(data);
      }
      }
      loadSankey()
