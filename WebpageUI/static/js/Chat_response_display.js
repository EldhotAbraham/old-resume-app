function response_displayFunChat() {
    $('#nlpResponse').show();
    $('#loader_div').show();
    $('#intent_table').empty();
    $('#stage_table').empty();
    $('#sentimentdiv').empty();
    $('#ajax_result').empty();
    p_text = $("#parserId").val();
    var formData = new FormData()
    formData.append('text_body', p_text);
    var url = $("#btn_hid").data('url');
    console.log(url);
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function(data, textStatus, jqXHR) {
            $("#loader_div").hide();
            $('#intent_table').empty();
            $('#stage_table').empty();
            $('#sentimentdiv').empty();
            console.log(data);
            text = $("#parserId").val();
            Sentiment_data = []
            Entity_data = {}
            Intent_data = {}
            Sentiment_data = data['Sentiment_Analysis']
            Entity_data = data['Entity_recognition']
            Intent_data = data['Intent_Classification']
            Intent_display = []
            Intent_display = Intent_data['Response']
            console.log(Intent_display[0])
            /* We need to add the block of Appending in the UI */
            show_intent = Intent_display[0]
            $('#intent_table').append(show_intent)
            /* We need to add the block of Appending in the UI */

            entityDisplay(Entity_data);
            console.log((Sentiment_data.length))
//            graphCheckLength = Sentiment_data.length;
//
//            sentimentDisplay(Sentiment_data);
            $('#response_table').show();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $('#ajax_result').empty();
            $("#loader_div").hide();
            $('#ajax_result').append("<b>" + "Error due to some technical issues for the response" + "</b>");
            console.log("Error due to some technical issues for the response from the NLP Engine")
        }
    });
}

function sentimentDisplay(Sentiment_data) {
    colorvar = '#7db343'
    console.log("**-*-*-*-*-*--", Sentiment_data[0]);
    sentiDict = [];
    sentiDict = Sentiment_data[0];
    console.log(sentiDict['value'])
    sentiScore = sentiDict['value']
    if (sentiScore > 0) {
        $('#sentimentdiv').append("Positive Sentence")
    } else if (sentiScore == 0) {
        $('#sentimentdiv').append("Neutral Sentence")
    } else {
        $('#sentimentdiv').append("Negative Sentence")
    }
}

function entityDisplay(data) {
    var intents = [];
    var colorCounter = 0;
    for (var k in Entity_data) intents.push(k)
    newText = text;
    for (var t = 0; t < intents.length; t++) {
        var intentData = Entity_data[intents[t]]
        for (dataCounter = 0; dataCounter < intentData.length; dataCounter++) {
            color_array = ['#1E90FF', '#ff5ab3', '#3F51B5', '#7db343', '#5bb899', '#3b78dc', '#e1aa02', '#0A8043'];
            var colorvar = color_array[colorCounter % 8];
            if (intents[t] == "AWBN") {
                rectLength = 180;
            } else {
                rectLength = (intentData[dataCounter].length) * 8 + (intents[t].length + 9) * 8;
            }
            var intentDisplay = intents[t].toUpperCase();

            var reUnderScore = new RegExp("_", "ig");
            intentDisplay = intentDisplay.replace(reUnderScore, " ");
            var reDates = new RegExp("_text", "ig");
            intentDisplay = intentDisplay.replace(reDates, " ");
            var reDesc = new RegExp("_desc", "ig");
            intentDisplay = intentDisplay.replace(reDesc, " ");

            console.log('......................', intentData[dataCounter]);
            replacedText = "<svg width=" + rectLength + " height='30'> <rect width=" + rectLength + " height='30' rx='5' ry='5' style='color:#FFD700;fill:" + colorvar + ";stroke-width:10;fill-opacity:0.6;stroke-opacity:0.9'></rect><text x='15' y='20' color='Grey'>" + intentData[dataCounter] + "\n &ndash;&gt; \n" + intentDisplay + "</text> </svg>"
            colorCounter++;
            str1 = intentData[dataCounter]
            console.log()
            var re = new RegExp(str1, "ig");
            newText = newText.replace(re, replacedText);
        }
    }
    $('#stage_table').append("<b>" + newText + "</b>");
}