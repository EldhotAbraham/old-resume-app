function drawWordCloud(scenario){
    var chart = am4core.create("word_cloud_voice_entity", am4plugins_wordCloud.WordCloud);
    var series = chart.series.push(new am4plugins_wordCloud.WordCloudSeries());

    var review_words = "'Airport','Shuttle Service','Cab','Delay', 'Cancelled','Hotel','Lounge','Terminal','Inflight Entertainment','One World Alliance','Star Alliance','Flights','AirbusA220','AirbusA300','AirbusA310','AirbusA318','AirbusA319','AirbusA320','AirbusA321','AirbusA330','Airbus340','AirbusA350','AirbusA380','ATR 42/72','Beech 1900D','Boeing 717','Boeing 737','Canadair Regional Jet','Comac ARJ21','Comac C919','Concorde','Dash 8','Embraer 120','Fokker 50','McDonnell Douglas DC-10','McDonnell Douglas MD-11','Saab 2000','Saab 340','Sukhoi SuperJet 100,'ERF','FRA','HAM','THF','CGN','DUS','MUC','NUE','LEJ','SCN','STR','TXL','BRE','HAJ','QWER13', 'Sydney','Qatar','Dubai',";
    series.accuracy = 4;
    series.step = 15;
    series.rotationThreshold = 0.7;
    series.maxCount = 200;
    series.minWordLength = 2;
    series.labels.template.margin(4,4,4,4);
    series.maxFontSize = am4core.percent(30);

//     var words ;
//        switch(scenario){
//            case 'chat':
//                words = chat_words;
//                break;
//             case 'mail':
//                words = mail_words;
//                break;
//             case 'voice':
//                words = voice_words;
//                break;
//             case 'review':
//                words = review_words;
//                break;
//        }
    series.text=review_words
    series.colors = new am4core.ColorSet();
    series.colors.passOptions = {}; // makes it loop

    //series.labelsContainer.rotation = 45;
    series.angles = [0,-90];
    series.fontWeight = "700"

    setInterval(function () {
      series.dataItems.getIndex(Math.round(Math.random() * (series.dataItems.length - 1))).setValue("value", Math.round(Math.random() * 300));
     }, 10000)
}
drawWordCloud('voice')
