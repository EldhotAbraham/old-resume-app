$(document).ready(function() {

    window.get_selected_skills = function get_selected_skills() {

        var skills_ids = [];
        $('#skill_container').children('.skill-block').each(function() {
            var existing_skill_id = $(this).children('input[name="skill_id"]').val();
//            console.log(existing_skill_id);
            if (existing_skill_id != null) {
                skills_ids.push(existing_skill_id);
            }
        });
        console.log(skills_ids,"&&&&&&&&&&&&&&&&&")
        return skills_ids;
    };

    $('#skill_container').on('click', '.skill-block', function() {
        //    var selected = $(this).children('input[name="skill_id"]').val()
        //    alert(selected);
        $(this).remove();
    })

    function addSkill(skill) {

        var status = 0
        $('#skill_container').children('.skill-block').each(function() {
            var existing_skill_id = $(this).children('input[name="skill_id"]').val();
//            console.log(existing_skill_id);
            if (skill.value == existing_skill_id) {
                status = 1
            }
        });
        if (status == 0) {
            var skill_segment = '<div class="skill-block">' +
                '<input type="hidden" name="skill_id" value="' + skill.value + '">' +
                '<img class="skill-block-close" src="/static/resumeparser/images/close.png">' +
                '<div class="skill-block-text">' + skill.label + '</div></div>'
            $('#skill_container').append(skill_segment)
        }
        $('.search-input').val('');
    }

    $(".tech-search").autocomplete({
        select: function(e, ui) {
            $("#box").empty();
            console.log("%%%%%",ui.item.label)
            $(".tech-search").val(ui.item.label);
            if (ui.item.mode == 0) {
                addSkill(ui.item);
            } else if (ui.item.mode == 1) {
                get_employee_with_name(ui.item.label);
                console.log("^^^^^^^^^^^^^^")
                $('.search-input').val('');
                $('#skill_container').empty();
            }
            return false;
        },

        appendTo: "#container",
        source: "TechnologyAutoComplete",
        minLength: 2,
        open: function() {

            setTimeout(function(d) {

                $('.ui-autocomplete').css('z-index', 99);

            }, 0);
        }
    });

    function getLocationBar(location_id) {

//        console.log('location_id: ', location_id);

        $.ajax({

            url: 'get_location_bar_info',
            data: {
                'location_id': location_id
            },

            success: function(data) {

                $("#location_bar").empty().append(data);
            }
        });
    }

    function get_employee_with_name(emp_name) {

//        console.log('emp name: ', emp_name);

        $.ajax({

            url: 'GetEmployeeWithName',
            data: {
                'emp_name': emp_name
            },

            success: function(data) {
                $("#box").empty().append(data);
            }
        });
    }

    function get_employee_with_id(emp_id) {

//        console.log('emp_id ', emp_id);

        $.ajax({

            url: 'GetEmployeeWithId',
            data: {
                'emp_id': emp_id
            },

            success: function(data) {

                $("#box").empty().append(data);
            }
        });
    }
});