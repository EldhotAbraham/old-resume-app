from rest_framework import serializers
from .models import Technology

class TechnologySerializer(serializers.ModelSerializer):

    technology = serializers.CharField(source='technology')
    digital = serializers.CharField(source='digital')
    apptype = serializers.CharField(source='apptype')
    category = serializers.CharField(source='category')
    status= serializers.CharField(source='status')
    app_final = serializers.CharField(source='app_final')

    class Meta:
        model = Technology
        # STANDARD WAY
        # fields = ("technology", "digital", "apptype", "category", "status", "app_final")

        # TRADITIONAL WAY TO CHANGE KEY/PARAMETER NAME
        fields = ("technology", "digital", "apptype","category","status","app_final")