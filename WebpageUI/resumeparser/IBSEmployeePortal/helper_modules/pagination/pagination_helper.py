
def create_page_section(segment_no, page_no, page_size, total_count):

    start_index = ( segment_no - 1 ) * page_size + 1

    end_index = start_index + page_size - 1

    page_info = {}

    page_info["page_no"] = page_no
    page_info["segment_no"] = segment_no
    page_info["segment_size"] = page_size
    page_info["total_count"] = page_segment_count(page_size, total_count)

    page_info["prev_page"] = None
    page_info["next_page"] = None

    page_info["first_page"] = None
    page_info["last_page"] = None

    page_info["start_index"] = start_index
    page_info["end_index"] = end_index

    if end_index <= total_count:

       page_info["start_index"] = start_index
       page_info["end_index"] = end_index

    else:

        page_info["start_index"] = start_index
        page_info["end_index"] = total_count
        end_index = total_count

    if segment_no > 1:

        page_info["prev_page"] = segment_no - 1

        if segment_no > 1:
           # FIRST BUTTON
           page_info["first_page"] = 1

    if segment_no * page_size < total_count:

        page_info["next_page"] = segment_no + 1
        # print('total_count: ',total_count)
        if end_index != total_count:
           # LAST BUTTON
           page_info["last_page"] = total_count

    list = []

    for i in range(start_index, end_index + 1):
        list.append(i)

    page_info["page_range"] = list

    # page_info["page_range_text"] = str(start_index * page_size) + " to " + str(end_index * page_size ) + " of " +

    # print(page_info)

    return page_info

def page_segment_count(page_count, total_count):

    import math
    segment_count = total_count /  page_count
    segment_count = math.ceil(segment_count)
    # print(math.ceil(segment_count))

    return segment_count