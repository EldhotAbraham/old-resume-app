import psycopg2
import re
import glob
from docx import Document
import spacy
from spacy.matcher import Matcher
from datetime import datetime
from itertools import groupby

conn = psycopg2.connect(database="resume_portal", host="localhost", user="postgres", password="manager123")
cur = conn.cursor()

conn0 = psycopg2.connect(database="resume_portal", host="localhost", user="postgres", password="manager123")
cur0 = conn0.cursor()

conn = psycopg2.connect(database="resume_portal", host="localhost", user="postgres", password="manager123")
cur2 = conn.cursor()


nlp_sm = spacy.load("en_core_web_sm")

def runcheck(path):

    if path:
        path_to_watch = '{}/*docx'
        path_to_watch = path_to_watch.format(path)
    success = 0
    emp_query = "SELECT ibs_empid FROM resumeparser_employee;"
    cur.execute(emp_query)
    emp_status = cur.fetchall()
    # print("Existing data of the Employees present in the DATABASE", emp_status)
    emp_data = []
    emp_about_all = []
    for name in glob.glob(path_to_watch):
        length = len(name.split("\\"))
        file_name = name.split("\\")
        name_final = file_name[length-1]

        eid = re.findall(r"[A-Z][-][0-9]+", name_final)
        eid = str(eid).strip('[](),''\'')
        # print('employee_id from the document', eid)

        if len(eid) > 0:
            doc = Document(name)
            resume_text = []
            for r in doc.paragraphs:  # Loop for saving a text for the about of the Employee
                if r.text is not None:
                    resume_text.append(r.text)
            res = [list(sub) for ele, sub in groupby(resume_text, key=lambda x: x.strip() != '') if ele]
            # print("*********** Printing the result ",res)
            res_final = [str(i).strip() for i in res]
            output_final_data = (",".join(res_final))
            remove_chars = ['[', ']', '\'', '\\t']
            for i in remove_chars:
                output_final_data = output_final_data.replace(i, '')
            # print("#########---------------------------------------------------------------------", output_final_data)

            emp_data.append(res)
            emp_check = 0
            # Date of the Resume last modified
            date_of_resume = doc.core_properties.modified
            date_of_resume = str(date_of_resume).split(" ")[0]
            for emp in emp_status:
                emp = ''.join(emp)
                # print("!!!!", emp, "@@@@", eid)
                if emp == eid:
                    # print("Duplication found...........NOW CHECKING FOR THE DATE ")
                    date_query = "SELECT date_of_resume,id FROM resumeparser_employee where ibs_empid = '%s';"
                    item = eid
                    date_result = date_query % item
                    cur.execute(date_result)
                    date_of_resumes_result = cur.fetchall()
                    emp_check = emp_check + 1
                    datetime_object = datetime.strptime(date_of_resume, '%Y-%m-%d').date()
                    for date, e_id in date_of_resumes_result:
                        if date < datetime_object:
                            # dATE CHECK
                            # print("###..........................chECK OF THE dATE vALID fOR THE rECORD",e_id )
                            delete_query = "DELETE FROM resumeparser_employee_skills where employee_id=%s;"
                            delete_db = delete_query % e_id
                            cur0.execute(delete_db)
                            delete_query1 = "DELETE FROM resumeparser_employee where id = %s;"
                            delete_db1 = delete_query1 % e_id
                            cur0.execute(delete_db1)
                            conn0.commit()
                            # print("AFTER DELETING THE OLD RECORD UPDATING IT WITH THE NEW RECORD")
                            # print(name, "Inside the GLOB Function of the particular file")
                            # For finding the VISA Details from the doc.paragraphs

                            # Denoting the name of the emp name from the Resume
                            emp_name = doc.paragraphs[0].text
                            bad_chars = ['-', '<', '>']
                            for i in bad_chars:
                                emp_name = emp_name.replace(i, '')
                            emp_name = emp_name.strip()
                            # print("The name of the emp found from the resume", emp_name)
                            # Noting the Role of the Emp from the resume
                            role = doc.paragraphs[1].text
                            bad_chars = ['<', '>']
                            for i in bad_chars:
                                role = role.replace(i, '')
                            role = role.strip()
                            # Contact Number noted
                            phone = doc.paragraphs[2].text
                            bad_chars = [';', '#', '-', ' ', '.', '+', ':', '<', '>']
                            for i in bad_chars:
                                phone = phone.replace(i, '')
                            phone = phone.strip()
                            # print("Phone number at 1st phase....", phone)
                            phone_array = phone.replace('\\', ' ').replace(',', ' ').split()
                            if phone.isdigit():  # Contact number validation
                                emp_contact = phone
                            elif phone.isalnum():
                                emp_contact = phone
                                emp_contact = re.findall(r'\d+', emp_contact)[0]
                            elif phone_array:
                                contact_no = phone_array[0]
                                if contact_no.isdigit():
                                    emp_contact = contact_no
                            else:
                                emp_contact = None
                            # About
                            emp_about = res[1]
                            if len(emp_about) < 2:
                                emp_about = res[2]
                            # If the Emp_About is showing up the wrong result we can apply the fetch_emp_about using the Spacy models.
                            emp_about = str(emp_about).strip('[](),''\'')
                            # print(emp_about, ">>>>>>>>.....About of the EMP....<<<<<<<<<")
                            emp_group = fetch_emp_group(output_final_data)
                            emp_group_data = sort_functional_location(emp_group)
                            if emp_group_data:
                                emp_group_datas = emp_group_data
                                emp_functional_group = str(emp_group_datas)
                            else:
                                emp_functional_group = 'others'
                            # print("............Inserting the Functional Group Of the Employee......",
                            #       emp_functional_group)
                            # VISA
                            visa_location = fetch_visalocation(output_final_data)
                            visa_data = sort_visa_location(visa_location)
                            if visa_data:
                                visa_data = str(visa_data)
                                emp_visa_location = str(visa_data)
                            else:
                                emp_visa_location = None
                            if visa_location:
                                visa = visa_location[0]
                            else:
                                visa = None
                            # print("............VISA LOCATION......", emp_visa_location)
                            # Insert Query into the DB of the Employees without previous existence
                            insert_query = "INSERT INTO resumeparser_employee(ibs_empid,emp_name,designation,about,contact,status," \
                                           " date_of_resume, functional_group, visa, visa_location)VALUES(%s,%s,%s,%s,%s,%s," \
                                           " %s, %s, %s, %s)"
                            record_insert = (
                            eid, emp_name, role, emp_about, emp_contact, True, date_of_resume, emp_functional_group,
                            visa, emp_visa_location)
                            cur2.execute(insert_query, record_insert)
                            conn.commit()
                            success = 1
                            emp_digitals_count = 0
                            tech1 = {}
                            can_proceed = False
                            # Into the Technology Table of the resume
                            for row in doc.tables[0].rows:
                                if len(row.cells) > 1:
                                    can_proceed = True
                                    if row.cells[0].text == "Technology":
                                        continue
                                    if row.cells[0].text == "":
                                        continue
                                    else:
                                        print("..............", row.cells[0].text)
                                    tech1.update({row.cells[0].text: row.cells[1].text})
                            if can_proceed is True:
                                items = []
                                # print("Inside the Can_proceed functionality")
                                # print(tech1, "TECH Dict --------")  # Dictionary having the Tech from the resume
                                for insert_tech in tech1:
                                    tech_details = tech1[insert_tech].split(",")
                                    for item in tech_details:
                                        # print(item, "Each Technology on specific")
                                        trimmed = re.sub(r"^\s+", "", item)
                                        items.append(trimmed)
                                for data in items:
                                    data = data.strip()
                                    # Query for finding the Tech ID from the technology table
                                    tech_query = "select id,digital from resumeparser_technology where lower(technology) = lower('%s')"
                                    tech_query = tech_query % data
                                    cur2.execute(tech_query)
                                    techid = cur2.fetchall()
                                    # print(techid, " The tech_id from the DB")
                                    techid = list(techid)
                                    # Query for finding the ID of the Employees from the emp table
                                    select_id = "select id from resumeparser_employee where ibs_empid='%s'"
                                    select_id = select_id % eid
                                    cur2.execute(select_id)
                                    employee_id = cur2.fetchall()
                                    employee_id = employee_id[0][0]
                                    # print("Table ID of the EMP", employee_id)

                                    for k in techid:
                                        # Query for getting the data from the emp_skill_DB
                                        x = "select id from resumeparser_employee_skills where employee_id='%s' and technology_id='%s'"
                                        record = (employee_id, k[0])
                                        cur2.execute(x, record)
                                        data = cur2.fetchall()
                                        # print("Query Result of the Skill DB", data)
                                        if data is None or len(
                                                data) == 0:  # Check made if the EMP Is noted in the Skills table
                                            # Query for inserting the EMP Skill details into the DB
                                            insert = "INSERT INTO resumeparser_employee_skills(employee_id,technology_id)VALUES(%s,%s)"
                                            record = (employee_id, k[0])
                                            cur2.execute(insert, record)
                                            conn.commit()
                                        if k[1] == 'YES':
                                            emp_digitals_count = emp_digitals_count + 1
                                    # print(emp_digitals_count, "^^^^^^^^^^Digital Count")
                                    if emp_digitals_count > 0:
                                        # Query for calculating the Digital Technologies
                                        d_query = " select id from resumeparser_technology where digital = '%s'"
                                        item = 'YES'
                                        digital_query = d_query % item
                                        cur2.execute(digital_query)
                                        digitals = cur2.fetchall()
                                        dq_denom = len(digitals) * 5
                                        dq_numerator = emp_digitals_count * 5
                                        digital_quotient = dq_numerator / dq_denom
                                        digital_quotient = digital_quotient * 100
                                        dq = round(digital_quotient, 2)
                                        update_dq_query = "UPDATE resumeparser_employee set digital_quotient='%s' where id='%s'"
                                        record = (dq, employee_id)
                                        cur2.execute(update_dq_query, record)
                                        conn.commit()
                        # else:
                        #     print("The Date of the resume is not new as compared to the DB NO CHANGES in the DB")
            if emp_check == 0:
                # print(name, "Inside the GLOB Function of the particular file")
                # For finding the VISA Details from the doc.paragraphs

                # Denoting the name of the emp name from the Resume
                emp_name = doc.paragraphs[0].text
                bad_chars = ['-', '<', '>']
                for i in bad_chars:
                    emp_name = emp_name.replace(i, '')
                emp_name = emp_name.strip()
                # print("The name of the emp found from the resume",emp_name)
                # Noting the Role of the Emp from the resume
                role = doc.paragraphs[1].text
                bad_chars = ['<', '>']
                for i in bad_chars:
                    role = role.replace(i, '')
                role = role.strip()
                # Contact Number noted
                phone = doc.paragraphs[2].text
                bad_chars = [';', '#', '-', ' ', '.', '+', ':', '<', '>']
                for i in bad_chars:
                    phone = phone.replace(i, '')
                phone = phone.strip()
                # print("Phone number at 1st phase....", phone)
                phone_array = phone.replace('\\', ' ').replace(',', ' ').split()
                if phone.isdigit():  # Contact number validation
                    emp_contact = phone
                elif phone.isalnum():
                    emp_contact = phone
                    emp_contact = re.findall(r'\d+',emp_contact)[0]
                elif phone_array:
                    contact_no = phone_array[0]
                    if contact_no.isdigit():
                        emp_contact = contact_no
                else:
                    emp_contact = None
                # About
                emp_about = res[1]
                if len(emp_about) < 2:
                        emp_about = res[2]
                # If the Emp_About is showing up the wrong result we can apply the fetch_emp_about using the Spacy models.
                emp_about = str(emp_about).strip('[](),''\'')
                # print(emp_about, ">>>>>>>>.....About of the EMP....<<<<<<<<<")
                emp_group = fetch_emp_group(output_final_data)
                emp_group_data = sort_functional_location(emp_group)
                if emp_group_data:
                    emp_group_datas = emp_group_data
                    emp_functional_group = str(emp_group_datas)
                else:
                    emp_functional_group = 'others'
                # print("............Inserting the Functional Group Of the Employee......", emp_functional_group)
                # VISA
                visa_location = fetch_visalocation(output_final_data)
                visa_data = sort_visa_location(visa_location)
                if visa_data:
                    visa_data = str(visa_data)
                    emp_visa_location = str(visa_data)
                else:
                    emp_visa_location = None
                if visa_location:
                    visa = visa_location[0]
                else:
                    visa = None
                # print("............VISA LOCATION......", emp_visa_location)
                # Insert Query into the DB of the Employees without previous existence
                insert_query = "INSERT INTO resumeparser_employee(ibs_empid,emp_name,designation,about,contact,status," \
                               " date_of_resume, functional_group, visa, visa_location)VALUES(%s,%s,%s,%s,%s,%s," \
                               " %s, %s, %s, %s)"
                record_insert = (eid, emp_name,role,emp_about, emp_contact, True, date_of_resume, emp_functional_group,
                                 visa, emp_visa_location)
                cur2.execute(insert_query, record_insert)
                conn.commit()
                success = 1
                emp_digitals_count = 0
                tech1 = {}
                can_proceed = False
                # Into the Technology Table of the resume
                for row in doc.tables[0].rows:
                    if len(row.cells) > 1:
                        can_proceed = True
                        if row.cells[0].text == "Technology":
                            continue
                        if row.cells[0].text == "":
                            continue
                        else:
                            print("..............", row.cells[0].text)
                        tech1.update({row.cells[0].text: row.cells[1].text})
                if can_proceed is True:
                    items = []
                    # print("Inside the Can_proceed functionality")
                    # print(tech1,"TECH Dict --------")  # Dictionary having the Tech from the resume
                    for insert_tech in tech1:
                        tech_details = tech1[insert_tech].split(",")
                        for item in tech_details:
                            # print(item,"Each Technology on specific")
                            trimmed = re.sub(r"^\s+", "", item)
                            items.append(trimmed)
                    for data in items:
                        data = data.strip()
                        # Query for finding the Tech ID from the technology table
                        tech_query = "select id,digital from resumeparser_technology where lower(technology) = lower('%s')"
                        tech_query = tech_query % data
                        cur2.execute(tech_query)
                        techid = cur2.fetchall()
                        # print(techid," The tech_id from the DB")
                        techid = list(techid)
                        # Query for finding the ID of the Employees from the emp table
                        select_id = "select id from resumeparser_employee where ibs_empid='%s'"
                        select_id = select_id % eid
                        cur2.execute(select_id)
                        employee_id = cur2.fetchall()
                        employee_id = employee_id[0][0]
                        # print("Table ID of the EMP", employee_id)

                        for k in techid:
                            # Query for getting the data from the emp_skill_DB
                            x = "select id from resumeparser_employee_skills where employee_id='%s' and technology_id='%s'"
                            record = (employee_id, k[0])
                            cur2.execute(x, record)
                            data = cur2.fetchall()
                            # print("Query Result of the Skill DB", data)
                            if data is None or len(data) == 0:  # Check made if the EMP Is noted in the Skills table
                                # Query for inserting the EMP Skill details into the DB
                                insert = "INSERT INTO resumeparser_employee_skills(employee_id,technology_id)VALUES(%s,%s)"
                                record = (employee_id, k[0])
                                cur2.execute(insert, record)
                                conn.commit()
                            if k[1] == 'YES':
                                emp_digitals_count = emp_digitals_count + 1
                    # print(emp_digitals_count,"^^^^^^^^^^Digital Count")
                    if emp_digitals_count > 0:
                        # Query for calculating the Digital Technologies
                        d_query = " select id from resumeparser_technology where digital = '%s'"
                        item = 'YES'
                        digital_query = d_query % item
                        cur2.execute(digital_query)
                        digitals = cur2.fetchall()
                        dq_denom = len(digitals) * 5
                        dq_numerator = emp_digitals_count * 5
                        digital_quotient = dq_numerator / dq_denom
                        digital_quotient = digital_quotient * 100
                        dq = round(digital_quotient, 2)
                        update_dq_query = "UPDATE resumeparser_employee set digital_quotient='%s' where id='%s'"
                        record = (dq, employee_id)
                        cur2.execute(update_dq_query, record)
                        conn.commit()
    # print("**************About of the Employees", emp_about_all)
    return success

def fetch_visalocation(doc):
    doc_text = nlp_sm(doc)
    airline_list = []
    airline_matcher = Matcher(nlp_sm.vocab)
    cur2.execute("""SELECT * FROM public.visa_location_resume;""")
    airline_codes = cur2.fetchall()
    airline_code_array = []
    airline_name_array= []
    for airline_code in airline_codes:
        airline_code_list = []
        if airline_code[0]:
            airline_code_list.append({'LOWER': (airline_code[0]).lower()})
            airline_code_array.append(airline_code_list)
        if airline_code[1]:
            airline_names = airline_code[1].rstrip().lower()
            airline_name_array.append(airline_names)

    phrase_patterns = airline_code_array
    airline_matcher.add('Airline_Code', None, *phrase_patterns)
    airline_matches = airline_matcher(doc_text)
    for match_id, start, end in airline_matches:
        string_id = nlp_sm.vocab.strings[match_id]  # get string representation
        span = doc_text[start:end]  # the matched span
        airline_list.append(span.text)

    airline_patterns = buildPatterns(airline_name_array)

    airline_matcher = airlinebuildMatcher(airline_patterns)

    airline_list.extend(SentenceMatcher(airline_matcher, doc))
    return airline_list

def buildPatterns(skills):
    pattern = []
    for skill in skills:
        pattern.append(skillPattern(skill))
    return list(zip(skills, pattern))

def skillPattern(skill):
    pattern = []
    for b in skill.split():
        pattern.append({'LOWER':b})
    return pattern

def airlinebuildMatcher(patterns):

    phrase_matcher = Matcher(nlp_sm.vocab)
    for pattern in patterns:
        phrase_matcher.add(pattern[0], on_match, pattern[1])
    return phrase_matcher


def on_match(matcher, doc, id, matches):
    return matches


def SentenceMatcher(phrase_matcher, text):

    doc = nlp_sm((text.lower()))
    matches = phrase_matcher(doc)
    match_list =[]
    for b in matches:
        match_id, start, end = b
        match_list.append(str(doc[start: end]))
    return set(match_list)

def sort_visa_location(data):
    visa_location_output = list(data)
    visa_location_output = [item.lower().strip() for item in visa_location_output]
    visa_location_output = set(visa_location_output)
    visa_location_code = list(visa_location_output)
    cur2.execute("""SELECT * FROM public.visa_location_resume;""")
    airline_codes = cur2.fetchall()
    airline_code_array = []
    airline_name_array= []
    for airline_code in airline_codes:
        airline_code_list = []
        if airline_code[0]:
            airline_code_list = airline_code[0].strip().lower()
            airline_code_array.append(airline_code_list)
        if airline_code[1]:
            airline_names = airline_code[1].strip().lower()
            airline_name_array.append(airline_names)
    visa_dict = dict(zip(airline_code_array,airline_name_array))
    # print("#######...NEW DICTIONARY FORMED",visa_dict,"^^^^^^^^^LIST TO BE COMPARED WITH",visa_location_code)
    output = [visa_dict.get(key) for key in visa_location_code if visa_dict.get(key)]
    output = set(output)
    output_final = [str(i) for i in output]
    output_final_data = (",".join(output_final))
    return output_final_data

def sort_functional_location(data):
    functional_group_output = list(data)
    output = [item.lower().strip() for item in functional_group_output]
    output = set(output)
    functional_group_output_final = list(output)
    cur2.execute("""SELECT * FROM public.emp_group_resume;""")
    airline_codes = cur2.fetchall()
    airline_code_array = []
    airline_name_array= []
    for airline_code in airline_codes:
        airline_code_list = []
        if airline_code[0]:
            airline_code_list = airline_code[0].strip().lower()
            airline_code_array.append(airline_code_list)
        if airline_code[1]:
            airline_names = airline_code[1].strip().lower()
            airline_name_array.append(airline_names)
    visa_dict = dict(zip(airline_code_array,airline_name_array))
    # print("#######...NEW DICTIONARY FORMED",visa_dict,"^^^^^^^^^LIST TO BE COMPARED WITH",visa_location_code)
    output = [visa_dict.get(key) for key in functional_group_output_final if visa_dict.get(key)]
    output = set(output)
    output_final = [str(i) for i in output]
    output_final_data = (",".join(output_final))
    return output_final_data

def empGroupbuildMatcher(patterns):

    phrase_matcher = Matcher(nlp_sm.vocab)
    for pattern in patterns:
        phrase_matcher.add(pattern[0], on_match, pattern[1])
    return phrase_matcher

def fetch_emp_group(doc):

    cur2.execute("""SELECT * FROM public.emp_group_resume;""")
    emp_groups = cur2.fetchall()
    emp_groups_array = []

    for empgroup in emp_groups:
        emp_groups_list = []
        if empgroup[0]:
            emp_group_name = empgroup[0].rstrip().lower()
            emp_groups_array.append(emp_group_name)

    empgroup_patterns = buildPatterns(emp_groups_array)

    emp_group_matcher = empGroupbuildMatcher(empgroup_patterns)

    emp_groups_list.extend(SentenceMatcher(emp_group_matcher, doc))
    return emp_groups_list

def fetch_emp_about(doc):
    emp_year = []
    emp_info = []
    nlp_room = spacy.load("models/base/emp_about")
    # print("Inside")
    doc = nlp_room(doc)
    for ent in doc.ents:
        if ent.label_ == 'Year Of Experience':
            year_exp = ent.text
            print(year_exp,"*********")
            if year_exp:
                emp_year.append(year_exp)
        if ent.label_ == 'About Info':
            about_info = ent.text
            print(about_info,"----------")
            if about_info:
                emp_info.append(about_info)
    return emp_year, emp_info