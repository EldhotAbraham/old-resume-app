from django.urls import path
from .views import search_tehnology,get_employees_with_skills,tablepage, get_techs,multisel,searchtable,skill, add_technology
from .views import get_employee_profile_details,check_page,runcheck,get_employee_with_name,get_employee_with_id, runbot, dashboard, runcheckCSV, add_technology_page_form, add_technology_db
from .IBSEmployeePortal.apis.ibs_employee_data_apis import getEmployeeSkills


urlpatterns = [
    path('index',search_tehnology, name='in'),
    path('search', search_tehnology, name='MyApps'),
    path('TechnologyAutoComplete', getEmployeeSkills, name='get-employee-skills'),
    path('GetEmployeeWithSkills', get_employees_with_skills, name='search-employee-by-skills'),
    # path('addtechnology',add_technology_page, name='addtechnology'),
    path('tablepage', tablepage, name='table_page'),
    path('gettechs',get_techs,name='get_techs'),
    path('multisel',multisel,name='multiselect'),
    path('searchtable',searchtable, name='search_table'),
    path('skill',skill,name='skill'),
    # path('addtech',add_technology, name='add_technology_code'),
    path('add_technology', add_technology, name='add_technology_code'),
    path('amchart',get_employee_profile_details, name='getprofiles'),
    path('check',check_page,name='checkpage'),
    path('runcheck',runcheck, name='run_check'),
    path('runcheckCSV', runcheckCSV, name='run_checkCSV'),
    path('GetEmployeeWithName',get_employee_with_name, name='get-with-name'),
    path('GetEmployeeWithId', get_employee_with_id, name='get-with-id'),
    path('runbot',runbot,name='runbot'),
    path('dashboard',dashboard,name="HomePage"),
    path('add_technology_page', add_technology_page_form, name="Add_Technology_Page"),
    path('add_technology_save', add_technology_db, name="Add_Technology_Save"),

]

