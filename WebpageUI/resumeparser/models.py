from django.db import models


class AppType(models.Model):

    app_type_name = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.app_type_name

class Technology(models.Model):
    """
        Model for store Technology
    """
    technology = models.CharField(max_length=200, null=False, blank=False)
    digital = models.CharField(max_length=200, null=True, blank=True)
    apptype = models.ManyToManyField(AppType, related_name='technology_list')
    category = models.CharField(max_length=200, null=True, blank=True)
    status = models.BooleanField(default=True)
    app_final = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.technology

class Employee(models.Model):
    """
    Model for store Employee details
    """
    ibs_empid= models.CharField(max_length=20,null=True, blank=True)
    emp_name = models.CharField(max_length=200,null=True, blank=True)
    designation = models.CharField(max_length=200,null=True, blank=True)
    about = models.CharField(max_length=100000,null=True, blank=True)
    contact = models.CharField(max_length=100000,null=True, blank=True)
    status = models.BooleanField(default=True)
    skills = models.ManyToManyField(Technology, related_name='emplist_list')
    digital_quotient = models.FloatField(null=True, blank=True, default=None)
    date_of_resume = models.DateField(null=True, blank=True)
    functional_group = models.CharField(max_length=200,null=True, blank=True)
    visa = models.CharField(max_length=200,null=True, blank=True)
    visa_location = models.CharField(max_length=200,null=True, blank=True)

    def __str__(self):
        return self.ibs_empid




