from django import forms
from .models import Technology


class AddTechForm(forms.ModelForm):
    class Meta:
        model = Technology
        fields = ['technology', 'digital', 'category', 'status', 'app_final']
