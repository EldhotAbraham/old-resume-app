from .models import Technology,AppType,Employee
from django.shortcuts import render_to_response
from .IBSEmployeePortal.constants import PageConfig as PageSettings
from .IBSEmployeePortal.helper_modules.pagination.pagination_helper import create_page_section, page_segment_count
from .data_serializer import TechnologySerializer
from django.db.models import Q
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.db.models.functions import Upper
from django.contrib.auth import authenticate, login
from datetime import datetime
from .forms import AddTechForm
import textdistance
from django.http import JsonResponse
from .pagination_helper import create_page_section, page_segment_count

user_list = User.objects.get_queryset().order_by('id')
paginator = Paginator(user_list, 10)


# ..................*****************************************
# ....Adding functionality....
# ..................*****************************************
def add_technology(request):

    p_technology = request.POST.get('technology')
    p_digital = request.POST.get('digital')
    p_category = request.POST.get('category')
    p_app_type_ids = request.POST.getlist('apptype[]')
    p_status_str = request.POST.get('status')
    p_final = request.POST.get('app_final')

    can_proceed = True
    err_msg = ''

    if string_null(p_technology):
        can_proceed = False
        err_msg = 'Please enter the technology name.'

    if string_null(p_digital) and can_proceed == True:
        can_proceed = False
        err_msg = 'Please enter the digital option.'

    if len(p_app_type_ids) == 0 and can_proceed == True:
        can_proceed = False
        err_msg = 'Please enter the app types.'

    if string_null(p_category) and can_proceed == True:
        can_proceed = False
        err_msg = 'Please enter the category option.'

    if string_null(p_status_str) and can_proceed == True:
        can_proceed = False
        err_msg = 'Please enter the status.'

    if string_null(p_final) and can_proceed == True:
        can_proceed = False
        err_msg = 'Please enter the final status.'

    if can_proceed == False:
        return JsonResponse({'resp': 1, 'err_msg':err_msg})


    p_app_status = False

    if p_status_str.lower() == 'true':
        p_app_status = True

    app_type_list = []

    for type_id in p_app_type_ids:
        app_type = AppType.objects.get(id=type_id)
        print(app_type)
        app_type_list.append(app_type)

    obj = Technology.objects.create(technology=p_technology,
                              digital=p_digital,
                              category=p_category,
                              status=p_app_status,
                              app_final=p_final)
    obj.apptype.set(app_type_list)
    obj.save()
    return JsonResponse({'resp':0, 'scs_msg':'Technology added successfully!'})


def string_null(ip_str):
    return ip_str is None or ip_str == ''

# ..................*****************************************
# ....Get details functionality....
# ..................*****************************************


def get_objects():

    return User.objects.all()


def spell_checker(text):
    technology_data = list(Technology.objects.order_by('id'))
    spell_alert = 0
    spell_correction = None
    # Spell Checking for unique entries
    text = text.upper()
    if technology_data:
        add_on = ["JAVA"]
        tech_final = add_on + technology_data
        for tech in tech_final:
            similarity_score = textdistance.levenshtein.distance(text, str(tech))
            # print(similarity_score)
            if similarity_score == 1:
                spell_alert = 1
                break;
            elif similarity_score == 2:
                similarity_percent = textdistance.levenshtein.normalized_similarity(text, str(tech))
                # print("Similarity Percent is : ", similarity_percent)
                if similarity_percent > 0.7:
                    spell_alert = 1
                    break
    if spell_alert == 1:
        if str(tech) == "JAVA":
            text = "CORE JAVA"
            spell_correction = 1
        else:
            spell_correction = 1
            text = str(tech)
    # Spell Checking Closed
    return text, spell_correction


def get_employees_with_skills(request):

    seg_num = int(request.GET.get('segment_no', None))
    page_num = int(request.GET.get('page_no', None))
    skill_ids = request.GET.getlist('skill_ids[]', None)
    search_text_value = request.GET.get('search_text', None)
    filters_dept_value = request.GET.getlist('departments[]', None)
    filters_visa_value = request.GET.getlist('visa[]', None)

    # print('Value of FILTERS = ', filters_dept_value, "%%%%", filters_visa_value)
    employee_list = []
    spell_correction = None
    spell_alert = 0
    if search_text_value:
        print('Value of the Search Input text Inside the Mode Functionality = ', search_text_value)
        search_value_list_or_final = []
        if ',' in search_text_value:  # Checking for the OR Operator
            search_value_list_or = search_text_value.split(',')
            if search_value_list_or:
                print(" Items to be searched for ", search_value_list_or)
                search_value_list_or = [item.upper().strip() for item in search_value_list_or]
                print(search_value_list_or, "***Into the Query***")
                for search in search_value_list_or:
                    search, spell_correction_tag = spell_checker(search)
                    if spell_correction_tag == 1:
                        spell_alert = 1
                    if search is not None:
                        search_value_list_or_final.append(search)
                if spell_alert == 1:
                    spell_correction = search_value_list_or_final
                employee_list_or = Employee.objects.annotate(emp_name_Upper=Upper('emp_name')).annotate(designation_Upper=Upper('designation')).filter(Q(skills__app_final__in=search_value_list_or_final) | Q(designation_Upper__in=search_value_list_or_final) | Q(ibs_empid__in=search_value_list_or_final) | Q(emp_name_Upper__in=search_value_list_or_final) | Q(skills__category__in=search_value_list_or_final) | Q(skills__technology__in=search_value_list_or_final)).distinct()
                print(employee_list_or, "Printing the List Of the Output got from the OR Functionality")
                employee_list = employee_list_or.order_by('ibs_empid')
        elif '&' in search_text_value:  # Checking for the AND Operator
            search_value_list_and_final = []
            search_value_list_and = search_text_value.split('&')
            if search_value_list_and:
                print(" Items to be searched for ", search_value_list_and)
                search_value_list_and = [item.upper().strip() for item in search_value_list_and]
                print(search_value_list_and, "***Into the Query***")
                for search in search_value_list_and:
                    search, spell_correction_tag = spell_checker(search)
                    if spell_correction_tag == 1:
                        spell_alert = 1
                    if search is not None:
                        search_value_list_and_final.append(search)
                if spell_alert == 1:
                    spell_correction = str(search_value_list_and_final)
                value = search_value_list_and_final[0]
                query_set_result = Employee.objects.annotate(emp_name_Upper=Upper('emp_name')).annotate(designation_Upper=Upper('designation')).filter(
                    Q(skills__app_final=value) | Q(designation_Upper=value) | Q(ibs_empid=value) | Q(
                        emp_name_Upper=value) | Q(skills__category=value) | Q(
                        skills__technology=value))
                search_value_list_and.pop(0)
                employee_list_and = query_set_result
                for value in search_value_list_and_final:
                    employee_list_and = employee_list_and.filter(
                    Q(skills__app_final=value) | Q(designation_Upper=value) | Q(ibs_empid=value) | Q(
                        emp_name_Upper=value) | Q(skills__category=value) | Q(
                        skills__technology=value))
                    print(employee_list_and)
                print("****************************************** Final Data ****", employee_list_and)
                print(employee_list_and, "Printing the List Of the Output got from the AND Functionality")
                employee_list = employee_list_and.order_by('ibs_empid').distinct()
        else:
            print(" Items to be searched for ", search_text_value)
            search_text_value = search_text_value.upper().strip()
            search_text_value, spell_alert = spell_checker(search_text_value)
            if spell_alert == 1:
                spell_correction = search_text_value
            if 'CORE JAVA' not in search_text_value:
                search_text_value = search_text_value.replace("JAVA", "CORE JAVA")
            print(search_text_value, "***Into the Query***")
            employee_list = Employee.objects.annotate(emp_name_Upper=Upper('emp_name')).annotate(designation_Upper=Upper('designation'))\
                .filter(Q(skills__app_final=search_text_value) | Q(ibs_empid=search_text_value)
                        | Q(designation_Upper=search_text_value) | Q(emp_name_Upper=search_text_value) |
                    Q(skills__category=search_text_value) | Q(skills__technology=search_text_value)).distinct()
            print(employee_list, "Printing the List Of the Output got from Search i.e not OR and AND Operator")
    if skill_ids:
        skill_obj_list = []
        for skill_id_param in skill_ids:
            skill_obj = Technology.objects.filter(id=skill_id_param)[0]
            skill_obj_list.append(skill_obj)

        for tech_obj in skill_obj_list:
            if len(employee_list) == 0:
                employee_list = Employee.objects.filter(skills__id=tech_obj.id)
            else:
                employee_list = employee_list.filter(skills__id=tech_obj.id)

    if filters_dept_value:
        employee_list = employee_list.filter(Q(functional_group__in=filters_dept_value))

    if filters_visa_value:
        employee_list = employee_list.filter(Q(visa_location__in=filters_visa_value))

    employee_list = employee_list.order_by('ibs_empid')
    total_emp_found = len(employee_list)
    print("@@@@@@@@@@@@@@@@",total_emp_found)
    paginator = Paginator(employee_list, PageSettings.search_page_size)

    try:
        obj = paginator.page(page_num)
    except PageNotAnInteger:
        obj = paginator.page(1)
    except EmptyPage:
        obj = paginator.page(paginator.num_pages)

    page_seg = create_page_section(seg_num, page_num, PageSettings.page_segment_size, page_segment_count(PageSettings.search_page_size,len(employee_list)))

    if spell_correction:
        if type(spell_correction) is list:
            spell_correction = str(set(spell_correction))
        bad_chars = ['[', ']', '\'', '{', '}']
        for i in bad_chars:
            spell_correction = spell_correction.replace(i, '')
    context = {'employee_list': obj, "page_info": page_seg, "user_info": request.user, "count_info": total_emp_found,"spell_correction_check":spell_alert, "spell_correction_info": spell_correction}
    print(context,"Data that is passed into the template")
    return render_to_response('resumeparser/components/employee_list.html', {'employee_info': context})



#technology table pagination

def get_techs(request):
    techlist = Technology.objects.all().order_by('id')
    seg_num = int(request.GET.get('segment_no', None))
    page_num = int(request.GET.get('page_no', None))
    # product_id = int(request.GET.get('product_id', None))

    object_list = Technology.objects.all()
    print('here',len(object_list))
    paginator = Paginator(techlist, PageSettings.table_page_size)

    try:
        obj = paginator.page(page_num)
    except PageNotAnInteger:
        obj = paginator.page(1)
    except EmptyPage:
        obj = paginator.page(paginator.num_pages)

    page_seg = create_page_section(seg_num, page_num, PageSettings.page_segment_size, page_segment_count(PageSettings.search_page_size,len(object_list)))

    print(obj)

    context = {'users': obj, "page_info": page_seg}

    return render_to_response('resumeparser/components/table_page.html', { 'comment_info': context })

def get_employee_with_id(request):
    emp_id = request.GET.get('emp_id')
    employee_with_id = Employee.objects.filter(ibs_empid=emp_id).distinct()
    total_emp_found = len(employee_with_id)
    # print("@@@@@@@@@@@@@@@@", total_emp_found)
    context = {'employee_list': employee_with_id, "count_info": total_emp_found}

    return render_to_response('resumeparser/components/employee_list.html', {'employee_info': context})


def get_employee_profile_details(request):
    emp_id= request.GET.get('id', None)
    employee_id = Employee.objects.get(ibs_empid=emp_id)
    digitals = Technology.objects.all()

    dig = []
    for item in digitals:
        if item.digital == 'Yes':
            dig.append(item.technology)

    employees = Employee.objects.filter(ibs_empid=employee_id)

    emp = Employee.objects.filter(ibs_empid =emp_id)[0]
    dq = emp.digital_quotient
    emp_value = 500
    skill_value = 200
    final_value = 200

    emp_list = []
    emp_dict = {}
    emp_dict["name"] = emp.emp_name
    emp_dict["value"] = emp_value

    emp_children_list = []
    emp_final_value_dict = {}

    for tech in emp.skills.all():
        dict = {}
        dict["name"] = tech.technology
        dict["value"] = skill_value
        dict["children"] = []

        if tech.app_final in emp_final_value_dict.keys():
            emp_final_value_dict[tech.app_final]["children"].append(dict)
        else:
            emp_final_value_dict[tech.app_final] = {"name": tech.app_final, "value": final_value, "children": [dict]}

    for k, v in emp_final_value_dict.items():
        emp_children_list.append(v)

    emp_dict["children"] = emp_children_list
    emp_list.append(emp_dict)

    return render(request, 'resumeparser/profile.html', {"context": emp_list,"dq":dq,'empdata':employees, 'user_info':request.user})

def get_employee_with_name(request):

    emp_name = request.GET.get('emp_name')
    employee = Employee.objects.filter(emp_name = emp_name)
    # print("Details Of the Employee",employee)
    total_emp_found = len(employee)
    # print("@@@@@@@@@@@@@@@@", total_emp_found)
    context = {'employee_list': employee, "user_info": request.user, "count_info": total_emp_found}
    # print("###########...CONTEXT...##############",context)
    return render_to_response('resumeparser/components/employee_list.html', {'employee_info': context})


def tablepage(request):

    techlist = Technology.objects.get_queryset().order_by('id')
    paginator = Paginator(techlist, 10)

    object_list = Technology.objects.all()
    paginator = Paginator(object_list, PageSettings.page_size)
    try:
        obj = paginator.page(1)
    except PageNotAnInteger:
        obj = paginator.page(1)
    except EmptyPage:
        obj = paginator.page(paginator.num_pages)

    from .pagination_helper import create_page_section, page_segment_count
    page_seg = create_page_section(1, 1, PageSettings.page_segment_size,
                                   page_segment_count(PageSettings.page_size, len(object_list)))

    context = {'users': obj, "page_info": page_seg}

    return render(request, 'resumeparser/testing.html', {"context": context})

# ..................*****************************************
# ....Run BOT functionality....
# ..................*****************************************
def runbot(request):
    return render(request,'resumeparser/runbot.html',context={"active_menu":"runbot"})

def dashboard(request):
    return render(request,'main/MainHomePage.html',context={"user_info": request.user, "active_menu":"home"})

def check_page(request):
    return render(request,'resumeparser/running_check.html',context={})

def runcheck(request):
    from resumeparser.check_copy import runcheck
    path = request.POST.get('path')
    db_entry = runcheck(path)
    if db_entry == 1:
        messages.success(request, 'Successfully inserted the Data into the DB.')
    return render(request, 'resumeparser/runbot.html', context={"active_menu":"runbot"})

def runcheckCSV(request):
    from resumeparser.csvdata import runcheckcsv
    path = request.POST.get('path')
    date = request.POST.get('date')
    datetime_object = datetime.strptime(date, '%Y-%m-%d').date()
    # print(path,"@@@@@@",datetime_object)
    db_entry_csv = runcheckcsv(path,datetime_object)
    if db_entry_csv == 1:
        messages.success(request, 'Successfully inserted the Data into the DB.')
    return render(request, 'resumeparser/runbot.html', context={"active_menu":"runbot"})


# ..................*****************************************
# ....Search functionality....
# ..................*****************************************
def search_tehnology(request):

    visa_list = Employee.objects.order_by('visa_location').filter(~Q(visa_location__contains=',') & ~Q(visa_location__isnull=True)).values_list('visa_location', flat=True).distinct()
    department_list = Employee.objects.order_by('functional_group').values_list('functional_group', flat=True).distinct()

    context = {"visa_info": visa_list, "department_info": department_list}
    return render(request, 'resumeparser/search.html', {'context': context, "active_menu":"search"})


def multisel(request):
    return render(request,'resumeparser/selectbox.html',context={})


def searchtable(request):

    tech_list = Technology.objects.all()
    tech_dict ={}
    for item in tech_list:
        tech_dict.update({"technology":item.technology,"digital":item.digital,"apptype":item.apptype,"category":item.category,"status":item.status,"appfinal":item.app_final})

    print(tech_dict)
    return render(request,'resumeparser/searchtable.html',context=tech_dict)

def skill(request):

    query_set = Technology.objects.all()
    technology_list = TechnologySerializer(query_set, many=True).data

    import json
    technology_list_json  = json.loads(json.dumps(technology_list))

    context = {"tech_list":technology_list_json}

    print (technology_list_json)

    return render(request, 'resumeparser/searchtable.html', {"context": context})


def add_technology_page_form(request):
    techology_data = Technology.objects.order_by('id')
    print("--------------------",techology_data)
    form = AddTechForm()

    context = {"techology_data": techology_data, "form": form}
    print("Data passed........",context)
    return render(request, 'resumeparser/addtechnology_page.html', {'context': context,"active_menu":"technology"})


def add_technology_db(request):
    tech_data = AddTechForm(request.POST)
    print("**********",tech_data)
    if tech_data.is_valid():
        data = tech_data.save()
        messages.success(request, 'Successfully inserted the Data into the DB.')
    techology_data = Technology.objects.order_by('id')
    form = AddTechForm()
    context = {"techology_data": techology_data, "form": form}
    return render(request, 'resumeparser/addtechnology_page.html', {'context': context})
