from django.shortcuts import render

# Create your views here.
from django.shortcuts import render

def login(request):
   return render(request, "registration/login.html", {})

def register(request):
   return render(request, "user/register.html", {})

def index(request):
   return render(request, "main/MainHomePage.html", {})
